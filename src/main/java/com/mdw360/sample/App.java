package com.mdw360.sample;

import com.mdw360.sample.bean.Person;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        System.out.println( "Hello World!" );
        Writer writer = Files.newBufferedWriter(Paths.get("/tmp/test.csv"));

        ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
        mappingStrategy.setType(Person.class);

        StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
                .withApplyQuotesToAll(false)
                .withSeparator(',')
                .build();

        List<Person> personList = new ArrayList<Person>();
        Person p = new Person("Ja,ck", "Smith", "Toronto");
        Person p1 = new Person("James", "Tiff", "Ottawa");
        Person p2 = new Person("Penny", "Tole's", "Calgary");

        personList.add(p);
        personList.add(p1);
        personList.add(p2);

        beanToCsv.write(personList);
        writer.close();
    }
}
